<?php 
function head($title) { ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?=$title?></title>
        <link rel="stylesheet" href="/css/bootstrap.min.css">
    </head>
    <body>
        <nav class="navbar navbar-inverse" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Aduan SPR</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="menu">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/aduan">Aduan</a></li>
                    <li class="active"><a href="#">Selengara</a></li>
                </ul>
                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Cari">
                    </div>
                </form>
                <form class="navbar-form navbar-left hide" role="login">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Kataluluan">
                    </div>
                    <button type="submit" class="btn btn-default">Login</button>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Anton Heryanto</a></li>
                    <li><a href="#">Keluar</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
<?php
}

function foot() { ?>
<footer class="navbar navbar-inverse navbar-fixed-bottom" role="footer">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#footer">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">SPR &copy;2013</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="footer">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Hubungi Kami</a></li>
                    <li><a href="#">Terma dan Syarat</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </footer>

        <script src="/js/jquery-1.10.2.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
    </body>
</html>
        
<?php }
