<?php
include './ui.php';
include './orm.php';
include './aduan/api.php';
$title = "Borang Aduan";
head($title); ?>
<div class="container">
    <div class="panel panel-info">
        <div class="panel-heading">Borang Aduan</div>
        <form class="form-horizontal panel-body" method="POST">
            <div class="form-group <?=empty($error['title']) ? 'has-success' : 'has-error'?>">
                <label class="col-md-2 control-label">Perkara</label>
                <div class="col-md-10">
                    <input class="form-control"  name="title" value="<?=$f['title']?>">
                    <span class="help-block"><?=$error['title']?></span>
                </div>
            </div>
            <div class="form-group <?=empty($error['body']) ? 'has-success' : 'has-error'?>">
                <label class="col-md-2 control-label">Aduan</label>
                <div class="col-md-10">
                    <textarea class="form-control" name="body"><?=$f['body']?></textarea>
                    <span class="help-block"><?=$error['body']?></span>
                </div>
            </div>
            <div class="form-group <?=empty($error['name']) ? 'has-success' : 'has-error'?>">
                <label class="col-md-2 control-label">Nama</label>
                <div class="col-md-10">
                    <input class="form-control"  name="name" value="<?=$f['name']?>">
                    <span class="help-block"><?=$error['name']?></span>
                </div>
            </div>
            <div class="form-group has-error <?=empty($error['email']) ? 'has-success' : 'has-error'?>">
                <label class="col-md-2 control-label">Email</label>
                <div class="col-md-10">
                    <input class="form-control" name="email" value="<?=$f['email']?>">
                    <span class="help-block"><?=$error['email']?></span>
                </div>
            </div>
            <div class="form-group <?=empty($error['ic']) ? 'has-success' : 'has-error'?>">
                <label class="col-md-2 control-label">IC</label>
                <div class="col-md-10">
                    <input class="form-control" name="ic" value="<?=$f['ic']?>">
                    <span class="help-block"><?=$error['ic']?></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <button class="btn btn-primary">Hantar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php 
foot();