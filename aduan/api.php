<?php
if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $f = $_POST;
    $r = ['title','body','email'];
    $error = [];
    foreach ($r as $k) {
        if (empty($f[$k])) {
            $error[$k] = 'wajib diisi';
        }
    }
    if (sizeof($error) === 0) {
        save($f, $error);
    }
    
    var_dump($f);
}  
function load() {
    $db = new Db();
    $c = $db->query("select * from ticket t left join user u on u.id=t.user_id");
    $r = $c->fetchAll();
    $c->closeCursor();
    return $r;
}

function save(&$f, &$error) {
    $u = new Orm('user');
    $h = $u->query("select id from user where email=:email", [":email" => $f['email']]);
    $id = $h->fetchColumn();
    $h->closeCursor();
    if (!$id && empty($f['name'])) {
        $error['name'] = 'email not found, please provided name';
        return;
    }
    if (!$id) {
        $id = $u->insert([
            "email" => $f['email'],
            "name" => $f['name'],
            "ic" => $f['ic'],
            "created" => $_SERVER['REQUEST_TIME']
        ]);
    }
    $t = new Orm("ticket");
    echo "id: $id ";
    $y = $t->insert([
        "user_id" => $id,
        "title" => $f['title'],
        "body" => $f['body'],
        "created" => $_SERVER['REQUEST_TIME']
    ]);
    $f = [];
}